package in.nareshit.raghu.service;

import in.nareshit.raghu.entity.Specialization;

public interface ISpecializationService {

	Long saveSpecialization(Specialization obj);
}
